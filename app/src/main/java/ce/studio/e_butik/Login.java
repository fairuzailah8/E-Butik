package ce.studio.e_butik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends Fragment {

    Button btnregister,btnlogin;

    private static final String TAG_RESULT = "result";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_IDUSER = "iduser";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_TELEPON = "telepon";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    EditText email,password;

    ProgressDialog pd;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_login, container, false);

        email = (EditText) rootview.findViewById(R.id.email);
        password = (EditText) rootview.findViewById(R.id.password);
        btnlogin = (Button) rootview.findViewById(R.id.btnlogin);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new login().execute();
            }
        });

        return rootview;
    }

    private class login extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.LOGIN_URL;
            FEED_URL += "?email="+email.getText().toString();
            FEED_URL += "&password="+password.getText().toString();

            Log.e("Login =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String login = a.getString(TAG_LOGIN);

                        if (login.equals("1")){
                            String iduser = a.getString(TAG_IDUSER);
                            String nama = a.getString(TAG_NAMA);
                            String email = a.getString(TAG_EMAIL);
                            String telepon = a.getString(TAG_TELEPON);

                            SharedPreferences.Editor editor = getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                            editor.putString("statlogin", "1");
                            editor.putString("iduser", iduser);
                            editor.putString("nama", nama);
                            editor.putString("email", email);
                            editor.putString("telepon", telepon);
                            editor.commit();

                            Intent intent = getActivity().getBaseContext().getPackageManager()
                                    .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        else{
                            Toast.makeText(getActivity(), "Username/Password tidak terdaftar atau belum diverifikasi admin", Toast.LENGTH_SHORT).show();
                        }
                        pd.hide();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getActivity(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }
}
