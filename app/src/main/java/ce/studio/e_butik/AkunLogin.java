package ce.studio.e_butik;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AkunLogin extends Fragment {

    ImageView btnlogout;

    TextView nama,email;

    LinearLayout keranjang,pesanan,tentang,keluar;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_akun_login, container, false);

        nama = (TextView) rootview.findViewById(R.id.nama);
        email = (TextView) rootview.findViewById(R.id.email);
        keranjang = (LinearLayout) rootview.findViewById(R.id.keranjang);
        pesanan = (LinearLayout) rootview.findViewById(R.id.pesanan);
        tentang = (LinearLayout) rootview.findViewById(R.id.tentang);
        keluar = (LinearLayout) rootview.findViewById(R.id.keluar);

        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.clear();
                editor.commit();

                Intent intent = getActivity().getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        keranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Keranjang.class);
                startActivity(intent);
            }
        });

        pesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Pesanan.class);
                startActivity(intent);
            }
        });

        tentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Tentang.class);
                startActivity(intent);
            }
        });

        nama.setText(getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("nama",""));
        email.setText(getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("email",""));

        return rootview;
    }
}
