package ce.studio.e_butik;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Keranjang extends AppCompatActivity {

    public static Activity fa;

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDPRODUK = "idproduk";
    private static final String TAG_NAMAPRODUK = "namaproduk";
    private static final String TAG_HARGAPRODUK = "hargaproduk";
    private static final String TAG_BAHANPRODUK = "bahanproduk";
    private static final String TAG_GAMBAR = "gambar";
    private static final String TAG_JUMLAHPESAN = "jumlahpesan";
    private static final String TAG_TOTALPESANAN = "totalpesanan";
    private static final String TAG_KETERANGAN = "keterangan";

    public static String FEED_URL1;

    ProgressDialog pd;

    private GridView mGridView1;
    private GridViewAdapterDaftarKeranjang mGridAdapterProduk;
    private ArrayList<GridItemDaftarProduk> mGridDataProduk;

    private static final String TAG = DaftarProduk.class.getSimpleName();

    static boolean a=false;

    JSONArray rs = null;
    String myJSON="";
    public static boolean close=false;

    Button btncheckout;

    public static TextView hargatotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keranjang);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fa = this;

        close=false;

        hargatotal = (TextView) findViewById(R.id.hargatotal);
        mGridView1 = (GridView) findViewById(R.id.gridView1);

        btncheckout = (Button) findViewById(R.id.btncheckout);

        //Start download
        new getKeranjang().execute();

        btncheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GridViewAdapterDaftarKeranjang.totalpesanan==0){
                    Toast.makeText(Keranjang.this, "Anda belum memilih produk", Toast.LENGTH_SHORT).show();
                }
                else{
                    startActivity(new Intent(Keranjang.this,OpsiAlamat.class));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
            close=true;
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        finish();
        close=true;
    }

    private class getKeranjang extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL1 = Config.DAFTARKERANJANG_URL;
            FEED_URL1 += "?iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");

//            Log.e("URL Keranjang =",FEED_URL1);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL1);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
//                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    GridItemDaftarProduk item1;

                    if(String.valueOf(rs).equals(myJSON)){

                    }
                    else{
                        //Initialize with empty data
                        mGridDataProduk = new ArrayList<>();
                        mGridAdapterProduk = new GridViewAdapterDaftarKeranjang(Keranjang.this, R.layout.grid_item_daftar_keranjang, mGridDataProduk);
                        mGridView1.setAdapter(mGridAdapterProduk);
                        hargatotal.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((String.valueOf("0"))))).replace(",","."));

                        mGridDataProduk.clear();

//                        pd = new ProgressDialog(Keranjang.this);
//                        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
//                        pd.setCancelable(false);
//                        pd.show();

                        for (int i = 0; i < rs.length(); i++) {
                            JSONObject post1 = rs.optJSONObject(i);
                            String stridproduk = post1.getString(TAG_IDPRODUK);
                            String strnamaproduk = post1.getString(TAG_NAMAPRODUK);
                            String strhargaproduk = post1.getString(TAG_HARGAPRODUK);
                            String strbahanproduk = post1.getString(TAG_BAHANPRODUK);
                            String strjumlahpesan = post1.getString(TAG_JUMLAHPESAN);
                            String strtotalpesanan = post1.getString(TAG_TOTALPESANAN);
                            String strketerangan = post1.getString(TAG_KETERANGAN);
                            item1 = new GridItemDaftarProduk();
                            item1.setIdproduk(stridproduk);
                            item1.setNama(strnamaproduk);
                            item1.setHarga(strhargaproduk);
                            item1.setBahan(strbahanproduk);
                            item1.setJumlahpesan(strjumlahpesan);
                            item1.setTotalpesanan(strtotalpesanan);
                            item1.setKeterangan(strketerangan);
                            if (null != rs && rs.length() > 0) {
                                JSONObject attachment = rs.getJSONObject(i);
                                if (attachment != null)
                                    item1.setGambar(attachment.getString(TAG_GAMBAR));
                            }

                            mGridDataProduk.add(item1);
                        }

                        mGridAdapterProduk.setGridData(mGridDataProduk);
                        myJSON=String.valueOf(rs);
                        GridViewAdapterDaftarKeranjang.totalpesanan=0;
                        GridViewAdapterDaftarKeranjang.listKeranjang.clear();
//                        pd.hide();
                    }
                    if(close){

                    }
                    else{
                        new getKeranjang().execute();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }
}
