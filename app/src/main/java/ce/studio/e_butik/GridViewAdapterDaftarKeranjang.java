package ce.studio.e_butik;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class GridViewAdapterDaftarKeranjang extends ArrayAdapter<GridItemDaftarProduk> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemDaftarProduk> mGridData = new ArrayList<GridItemDaftarProduk>();

    public static ArrayList<String> listKeranjang = new ArrayList<String>();

    public static int totalpesanan=0;
    public static int jumlahpesanan=0;

    GridItemDaftarProduk item;

    public GridViewAdapterDaftarKeranjang(Context mContext, int layoutResourceId, ArrayList<GridItemDaftarProduk> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemDaftarProduk> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idproduk = (TextView) row.findViewById(R.id.idproduk);
            holder.namaproduk = (TextView) row.findViewById(R.id.namaproduk);
            holder.hargaproduk = (TextView) row.findViewById(R.id.hargaproduk);
            holder.txthargaproduk = (TextView) row.findViewById(R.id.txthargaproduk);
            holder.bahanproduk = (TextView) row.findViewById(R.id.bahanproduk);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            holder.txtgambar = (TextView) row.findViewById(R.id.txtgambar);
            holder.jumlahpesan = (TextView) row.findViewById(R.id.jumlahpesan);
            holder.txtjumlahpesan = (TextView) row.findViewById(R.id.txtjumlahpesan);
            holder.keterangan = (TextView) row.findViewById(R.id.keterangan);
            holder.layoutkeranjang = (LinearLayout) row.findViewById(R.id.layoutkeranjang);
            holder.checkkeranjang = (CheckBox) row.findViewById(R.id.checkkeranjang);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        item = mGridData.get(position);
        holder.txtgambar.setText(Html.fromHtml(item.getGambar()));
        holder.idproduk.setText(Html.fromHtml(item.getIdproduk()));
        holder.namaproduk.setText(Html.fromHtml(item.getNama()));
        holder.hargaproduk.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((item.getHarga())))).replace(",","."));
        holder.txthargaproduk.setText(item.getHarga());
        holder.bahanproduk.setText(Html.fromHtml("Bahan : "+item.getBahan()));
        holder.jumlahpesan.setText(Html.fromHtml("Jumlah : "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((item.getJumlahpesan())))).replace(",",".")));
        holder.txtjumlahpesan.setText(Html.fromHtml(item.getJumlahpesan()));
        holder.keterangan.setText(Html.fromHtml(item.getKeterangan()));
        Glide.with(mContext).load(item.getGambar())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.gambar);

        holder.checkkeranjang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    listKeranjang.add(holder.idproduk.getText().toString());
                    SharedPreferences.Editor editor = getContext().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                    editor.putString("keranjang", listKeranjang.toString());
                    editor.commit();
                    Log.e("keranjang = ",getContext().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("keranjang",""));

                    totalpesanan+=Integer.parseInt(holder.txthargaproduk.getText().toString())*Integer.parseInt(holder.txtjumlahpesan.getText().toString());
                    jumlahpesanan+=Integer.parseInt(holder.txtjumlahpesan.getText().toString());
                    Keranjang.hargatotal.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((String.valueOf(totalpesanan))))).replace(",","."));
                }
                else{
                    listKeranjang.remove(holder.idproduk.getText().toString());
                    SharedPreferences.Editor editor = getContext().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                    editor.putString("keranjang", listKeranjang.toString());
                    editor.commit();
                    Log.e("keranjang = ",getContext().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("keranjang",""));
                    totalpesanan-=Integer.parseInt(holder.txthargaproduk.getText().toString())*Integer.parseInt(holder.txtjumlahpesan.getText().toString());
                    jumlahpesanan-=Integer.parseInt(holder.txtjumlahpesan.getText().toString());
                    Keranjang.hargatotal.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((String.valueOf(totalpesanan))))).replace(",","."));
                }

            }
        });

        holder.layoutkeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getContext().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idproduk", holder.idproduk.getText().toString());
                editor.putString("gambarproduk", holder.txtgambar.getText().toString());
                editor.putString("hargaproduk", holder.txthargaproduk.getText().toString());
                editor.putString("namaproduk", holder.namaproduk.getText().toString());
                editor.putString("bahanproduk", holder.bahanproduk.getText().toString());
                editor.putString("jumlahpesan", holder.txtjumlahpesan.getText().toString());
                editor.putString("keterangan", holder.keterangan.getText().toString());
                editor.commit();

                Quantity q = new Quantity((Activity)getContext());
                q.show();
            }
        });

        return row;
    }

    static class ViewHolder {
        TextView idproduk,namaproduk,hargaproduk,bahanproduk;
        ImageView gambar;
        TextView jumlahpesan,txtgambar,txthargaproduk,txtjumlahpesan,keterangan;
        CheckBox checkkeranjang;
        LinearLayout layoutkeranjang;
    }
}