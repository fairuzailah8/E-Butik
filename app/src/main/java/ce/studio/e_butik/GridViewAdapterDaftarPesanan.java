package ce.studio.e_butik;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class GridViewAdapterDaftarPesanan extends ArrayAdapter<GridItemPesanan> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemPesanan> mGridData = new ArrayList<GridItemPesanan>();

    GridItemPesanan item;

    public GridViewAdapterDaftarPesanan(Context mContext, int layoutResourceId, ArrayList<GridItemPesanan> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemPesanan> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idpesanan = (TextView) row.findViewById(R.id.idpesanan);
            holder.kodepesanan = (TextView) row.findViewById(R.id.kodepesanan);
            holder.namaproduk = (TextView) row.findViewById(R.id.namaproduk);
            holder.keterangan = (TextView) row.findViewById(R.id.keteranganpesanan);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            holder.hargapesan = (TextView) row.findViewById(R.id.hargapesan);
            holder.jumlahpesan = (TextView) row.findViewById(R.id.jumlahpesanan);
            holder.jumlahongkir = (TextView) row.findViewById(R.id.jumlahongkir);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        item = mGridData.get(position);
        holder.idpesanan.setText(Html.fromHtml(item.getIdpesanan()));
        holder.kodepesanan.setText(Html.fromHtml("#"+item.getKodepesanan()));
        holder.namaproduk.setText("Nama Produk = "+Html.fromHtml(item.getNamaproduk()));
        holder.keterangan.setText(Html.fromHtml(item.getStatuspesanan()));
        holder.hargapesan.setText("Harga Produk = Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((item.getHargapesan()))).replace(",",".")));
        holder.jumlahpesan.setText(Html.fromHtml(item.getJumlahpesan())+"X");
        holder.jumlahongkir.setText("Ongkir = Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((item.getTotalongkir()))).replace(",",".")));
        Glide.with(mContext).load(item.getGambar())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.gambar);

        return row;
    }

    static class ViewHolder {
        TextView idpesanan,kodepesanan,namaproduk,keterangan,hargapesan,jumlahpesan,jumlahongkir;
        ImageView gambar;
    }
}