package ce.studio.e_butik;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class OpsiPembayaran extends AppCompatActivity {

    public static Activity fa;

    EditText namarekeningpengirim,nomorrekeningpengirim,namabankpengirim;
    Button btnopsikonfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opsi_pembayaran);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fa = this;

        namarekeningpengirim = (EditText) findViewById(R.id.namarekeningpengirim);
        nomorrekeningpengirim = (EditText) findViewById(R.id.nomorrekeningpengirim);
        namabankpengirim = (EditText) findViewById(R.id.namabankpengirim);

        btnopsikonfirmasi = (Button) findViewById(R.id.btnopsikonfirmasi);

        btnopsikonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("namarekeningpengirim", namarekeningpengirim.getText().toString());
                editor.putString("nomorrekeningpengirim", nomorrekeningpengirim.getText().toString());
                editor.putString("namabankpengirim", namabankpengirim.getText().toString());
                editor.commit();

                startActivity(new Intent(OpsiPembayaran.this,OpsiKonfirmasi.class));
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

}
