package ce.studio.e_butik;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class Halaman_Utama extends AppCompatActivity {

    PagerAdapterHalamanUtamaNoLogin adapternologin;
    PagerAdapterHalamanUtamaLogin adapterlogin;

    EditText pencarian;

    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman__utama);

        pencarian = (EditText) findViewById(R.id.pencarian);

        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        if (getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")) {
            tabLayout.addTab(tabLayout.newTab().setText("Kategori"));
            tabLayout.addTab(tabLayout.newTab().setText("Produk"));
            tabLayout.addTab(tabLayout.newTab().setText("Saya"));
        }
        else{
            tabLayout.addTab(tabLayout.newTab().setText("Kategori"));
            tabLayout.addTab(tabLayout.newTab().setText("Produk"));
            tabLayout.addTab(tabLayout.newTab().setText("Saya"));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
            adapterlogin = new PagerAdapterHalamanUtamaLogin
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapterlogin);
        }
        else{
            adapternologin = new PagerAdapterHalamanUtamaNoLogin
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapternologin);
        }

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected
                    (TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        pencarian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Halaman_Utama.this, Pencarian.class));
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

}
