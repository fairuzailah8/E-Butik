package ce.studio.e_butik;

/**
 * Created by Artha on 2/2/2017.
 */
public class GridItemDaftarProduk {

    String idproduk,nama,harga,bahan,gambar,jumlahpesan,totalpesanan,keterangan;

    public String getJumlahpesan() {
        return jumlahpesan;
    }

    public void setJumlahpesan(String jumlahpesan) {
        this.jumlahpesan = jumlahpesan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getGambar() {


        return gambar;

    }

    public String getTotalpesanan() {
        return totalpesanan;
    }

    public void setTotalpesanan(String totalpesanan) {
        this.totalpesanan = totalpesanan;
    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }

    public String getIdproduk() {

        return idproduk;

    }

    public void setIdproduk(String idproduk) {

        this.idproduk = idproduk;

    }

    public String getNama() {

        return nama;

    }

    public void setNama(String nama) {

        this.nama = nama;

    }

    public String getHarga() {

        return harga;

    }

    public void setHarga(String harga) {

        this.harga = harga;

    }

    public String getBahan() {

        return bahan;

    }

    public void setBahan(String bahan) {

        this.bahan = bahan;

    }
}