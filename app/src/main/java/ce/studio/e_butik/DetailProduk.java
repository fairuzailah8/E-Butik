package ce.studio.e_butik;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import static java.security.AccessController.getContext;

public class DetailProduk extends AppCompatActivity {

    ExpandableHeightListView ListExpedisi;

    Spinner dataexpedisi,dataprovinsi,datakota;

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDPROVINSI= "idprovinsi";
    private static final String TAG_NAMAPROVINSI = "namaprovinsi";
    private static final String TAG_IDKOTA= "idkota";
    private static final String TAG_NAMAKOTA = "namakota";
    private static final String TAG_NAMASERVICE = "namaservice";
    private static final String TAG_ESTIMASI= "estimasi";
    private static final String TAG_BIAYA = "biaya";
    private static final String TAG_DESKRIPSIPRODUK = "deskripsiproduk";
    private static final String TAG_BAHANPRODUK = "bahanproduk";
    private static final String TAG_WARNAPRODUK = "warnaproduk";
    private static final String TAG_UKURANPRODUK = "ukuranproduk";
    private static final String TAG_BERATPRODUK = "beratproduk";
    private static final String TAG_STOKPRODUK = "stokproduk";
    private static final String TAG_HARGAPRODUK = "hargaproduk";
    private static final String TAG_KERANJANG = "keranjang";
    private static final String TAG_STATUSPRODUK = "statusproduk";
    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    public static String id,provinsi,kota,expedisi;

    private Integer gambar[] = {
            R.drawable.jne,
            R.drawable.tiki,
            R.drawable.pos
    };

    TextView deskripsiproduk,bahanproduk,warnaproduk,ukuranproduk,beratproduk,stokproduk,hargaproduk,btnbeli,nb;

    public static String strhargaproduk;

    ImageView btnkeranjang;

    public static Activity fa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa=this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        deskripsiproduk = (TextView) findViewById(R.id.deskripsiproduk);
        bahanproduk = (TextView) findViewById(R.id.bahanproduk);
        warnaproduk = (TextView) findViewById(R.id.warnaproduk);
        ukuranproduk = (TextView) findViewById(R.id.ukuranproduk);
        beratproduk = (TextView) findViewById(R.id.beratproduk);
        stokproduk = (TextView) findViewById(R.id.stokproduk);
        hargaproduk = (TextView) findViewById(R.id.hargaproduk);
        btnbeli = (TextView) findViewById(R.id.btnbeli);
        btnkeranjang = (ImageView) findViewById(R.id.btnkeranjang);
        nb = (TextView) findViewById(R.id.nb);

        dataexpedisi = (Spinner) findViewById(R.id.dataexpedisi);
        dataprovinsi = (Spinner) findViewById(R.id.dataprovinsi);
        datakota = (Spinner) findViewById(R.id.datakota);

        new getDataProduk().execute();

        ListExpedisi = (ExpandableHeightListView) findViewById(R.id.listexpedisi);

        ListExpedisi.setDivider(null);

        ListExpedisi.setExpanded(true);

        dataprovinsi.setVisibility(View.GONE);
        datakota.setVisibility(View.GONE);

        String[] iditem={"0","0","0"};
        String[] namaitem={"JNE","TIKI","POS"};
        SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),iditem,namaitem);
        dataexpedisi.setAdapter(customAdapter);

        dataexpedisi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                new getdataProvinsi().execute();
                dataprovinsi.setVisibility(View.VISIBLE);
                datakota.setVisibility(View.GONE);
                ListExpedisi.setVisibility(View.GONE);
                TextView tvexpedisi = (TextView) view.findViewById(R.id.namaitem);
                expedisi=tvexpedisi.getText().toString().replace(" ","%20");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dataprovinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataprovinsi.getSelectedItemPosition()==0){
                    datakota.setVisibility(View.GONE);
                    ListExpedisi.setVisibility(View.GONE);
                }
                else{
                    datakota.setVisibility(View.VISIBLE);
                    TextView tvprovinsi = (TextView) view.findViewById(R.id.iditem);
                    provinsi=tvprovinsi.getText().toString().replace(" ","%20");
                    new getdataKota().execute();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        datakota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (datakota.getSelectedItemPosition()==0){
                    ListExpedisi.setVisibility(View.GONE);
                }
                else{
                    TextView tvkota = (TextView) view.findViewById(R.id.iditem);
                    kota=tvkota.getText().toString().replace(" ","%20");
                    new getListExpedisi().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnbeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
                    QuantityBeliSekarang q = new QuantityBeliSekarang(DetailProduk.this);
                    q.show();
                }
                else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DetailProduk.this);
                    builder1.setMessage("Anda harus login untuk melakukan pemesanan");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Login",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(DetailProduk.this,NavigasiMasuk.class));
                                    finish();
                                }
                            });

                    builder1.setNegativeButton(
                            "Nanti",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });

        btnkeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
                    QuantityKeranjang q = new QuantityKeranjang(DetailProduk.this);
                    q.show();
                }
                else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DetailProduk.this);
                    builder1.setMessage("Anda harus login untuk menambahkan keranjang");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Login",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(DetailProduk.this,NavigasiMasuk.class));
                                    finish();
                                }
                            });

                    builder1.setNegativeButton(
                            "Nanti",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });

        nb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailProduk.this,Keranjang.class));
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private class getDataProduk extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.DETAILPRODUK_URL;
            FEED_URL += "?idproduk="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idproduk","");

            Log.e("URL Produk =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strdeskripsiproduk = a.getString(TAG_DESKRIPSIPRODUK);
                        String strbahanproduk = a.getString(TAG_BAHANPRODUK);
                        String strwarnaproduk = a.getString(TAG_WARNAPRODUK);
                        String strukuranproduk = a.getString(TAG_UKURANPRODUK);
                        String strberatproduk = a.getString(TAG_BERATPRODUK);
                        String strstokproduk = a.getString(TAG_STOKPRODUK);
                        strhargaproduk = a.getString(TAG_HARGAPRODUK);
                        String strstatusproduk = a.getString(TAG_STATUSPRODUK);

                        deskripsiproduk.setText(strdeskripsiproduk);
                        bahanproduk.setText(strbahanproduk);
                        warnaproduk.setText(strwarnaproduk);
                        ukuranproduk.setText(strukuranproduk);
                        beratproduk.setText(strberatproduk);
                        stokproduk.setText(strstokproduk);
                        hargaproduk.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((strhargaproduk)))).replace(",","."));

                        SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                        editor.putString("stokproduk", strstokproduk);
                        editor.commit();

                        if(strstatusproduk.equals("0")){
                            nb.setVisibility(View.GONE);
                            btnbeli.setVisibility(View.VISIBLE);
                            btnkeranjang.setVisibility(View.VISIBLE);
                        }
                        else{
                            nb.setVisibility(View.VISIBLE);
                            btnbeli.setVisibility(View.GONE);
                            btnkeranjang.setVisibility(View.GONE);
                        }

                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }

    private class getdataProvinsi extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.PROVINSI_URL;

            Log.e("URL Provinsi =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    String[] idprovinsi = new String[rs.length()];
                    String[] namaprovinsi = new String[rs.length()];

                    idprovinsi[0] = "0";
                    namaprovinsi[0] = "Pilih Provinsi";

                    for(int i=1; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String stridprovinsi = a.getString(TAG_IDPROVINSI);
                        String strnamaprovinsi = a.getString(TAG_NAMAPROVINSI);

                        idprovinsi[i] = stridprovinsi;
                        namaprovinsi[i] = strnamaprovinsi;

                    }
                    SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),idprovinsi,namaprovinsi);
                    dataprovinsi.setAdapter(customAdapter);

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }

    private class getdataKota extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.KOTA_URL;
            FEED_URL += "?idprovinsi="+provinsi;

            Log.e("URL Kota =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        String[] idkota = new String[rs.length()];
                        String[] namakota = new String[rs.length()];

                        idkota[0] = "0";
                        namakota[0] = "Pilih Kota";

                        for(int i=1; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String stridkota = a.getString(TAG_IDKOTA);
                            String strnamakota = a.getString(TAG_NAMAKOTA);

                            idkota[i] = stridkota;
                            namakota[i] = strnamakota;

                        }
                        SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),idkota,namakota);
                        datakota.setAdapter(customAdapter);
                    }
                    else{
                        String[] idkota = new String[1];
                        String[] namakota = new String[1];

                        idkota[0] = "0";
                        namakota[0] = "Pilih Kota";

                        SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),idkota,namakota);
                        datakota.setAdapter(customAdapter);
                    }


                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }

    private class getListExpedisi extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.BIAYA_URL;
            FEED_URL += "?destination="+kota;
            FEED_URL += "&beratsatuan=0.25";
            FEED_URL += "&jumlahbarang=1";
            FEED_URL += "&expedisi="+expedisi.toLowerCase();

            Log.e("URL Biaya =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        String[] namaservice = new String[rs.length()];
                        String[] hargaservice = new String[rs.length()];
                        String[] estimasiservice = new String[rs.length()];
                        Integer[] intgambar = new Integer[rs.length()];

                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String strnamaservice = a.getString(TAG_NAMASERVICE);
                            String strhargaservice = a.getString(TAG_BIAYA);
                            String strestimasiservice = a.getString(TAG_ESTIMASI);

                            namaservice[i] = strnamaservice;
                            hargaservice[i] = "Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((strhargaservice)))).replace(",",".");
                            if(!strestimasiservice.equals("")){
                                estimasiservice[i] = strestimasiservice+" hari";
                            }
                            if(expedisi.equals("JNE")){
                                intgambar[i] = gambar[0];
                            }
                            else if (expedisi.equals("TIKI")){
                                intgambar[i] = gambar[1];
                            }
                            else{
                                intgambar[i] = gambar[2];
                            }

                        }

                        ListviewAdapter customList = new ListviewAdapter(DetailProduk.this, namaservice, hargaservice, estimasiservice, intgambar);

                        ListExpedisi.setAdapter(customList);
                        ListExpedisi.setVisibility(View.VISIBLE);
                    }
                    else{
                        ListExpedisi.setVisibility(View.GONE);
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }
}
