package ce.studio.e_butik;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListviewAdapter extends ArrayAdapter<String> {
    private String[] namaservice;
    private String[] hargaservice;
    private String[] estimasiservice;
    private Integer[] gambar;
    private Activity context;

    public ListviewAdapter(Activity context, String[] namaservice, String[] hargaservice, String[] estimasiservice, Integer[] gambar) {
        super(context, R.layout.customlistview, namaservice);
        this.context = context;
        this.namaservice = namaservice;
        this.hargaservice = hargaservice;
        this.estimasiservice = estimasiservice;
        this.gambar = gambar;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.customlistview, null, true);
        TextView NamaService = (TextView) listViewItem.findViewById(R.id.tvNamaService);
        TextView HargaService = (TextView) listViewItem.findViewById(R.id.tvHargaService);
        TextView EstimasiService = (TextView) listViewItem.findViewById(R.id.tvEstimasiService);
        ImageView image = (ImageView) listViewItem.findViewById(R.id.gambarservice);

        NamaService.setText(namaservice[position]);
        HargaService.setText(hargaservice[position]);
        EstimasiService.setText(estimasiservice[position]);
        image.setImageResource(gambar[position]);
        return  listViewItem;
    }
}