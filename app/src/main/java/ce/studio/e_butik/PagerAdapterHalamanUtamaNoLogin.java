package ce.studio.e_butik;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapterHalamanUtamaNoLogin extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterHalamanUtamaNoLogin(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                DaftarKategori tab0 = new DaftarKategori();
                return tab0;
            case 1:
                DaftarProduk tab1 = new DaftarProduk();
                return tab1;
            case 2:
                AkunNoLogin tab2 = new AkunNoLogin();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}