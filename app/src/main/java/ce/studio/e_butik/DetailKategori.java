package ce.studio.e_butik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DetailKategori extends AppCompatActivity {

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDPRODUK = "idproduk";
    private static final String TAG_NAMAPRODUK = "namaproduk";
    private static final String TAG_HARGAPRODUK = "hargaproduk";
    private static final String TAG_BAHANPRODUK = "bahanproduk";
    private static final String TAG_GAMBAR = "gambar";

    public static String FEED_URL1;

    ProgressDialog pd;

    private GridView mGridView1;
    private GridViewAdapterDaftarProduk mGridAdapterProduk;
    private ArrayList<GridItemDaftarProduk> mGridDataProduk;

    private static final String TAG = DaftarProduk.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kategori);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mGridView1 = (GridView) findViewById(R.id.gridView1);

        FEED_URL1 = Config.DETAILKATEGORI_URL;
        FEED_URL1 += "?idkategori="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idkategori","");

        //Initialize with empty data
        mGridDataProduk = new ArrayList<>();
        mGridAdapterProduk = new GridViewAdapterDaftarProduk(DetailKategori.this, R.layout.grid_item_daftar_produk, mGridDataProduk);
        mGridView1.setAdapter(mGridAdapterProduk);

        pd = new ProgressDialog(DetailKategori.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        //Start download
        new getProduk().execute(FEED_URL1);

        mGridView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemDaftarProduk item = (GridItemDaftarProduk) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idproduk", item.getIdproduk());
                editor.putString("namaproduk", item.getNama());
                editor.putString("gambarproduk", item.getGambar());
                editor.putString("hargaproduk", item.getHarga());
                editor.putString("bahanproduk", item.getBahan());
                editor.commit();

                //Pass the image title and url to DetailsActivity
                Intent intent = new Intent(DetailKategori.this, DetailProduk.class);
                startActivity(intent);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    //Downloading data asynchronously
    public class getProduk extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result1 = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient1 = new DefaultHttpClient();
                HttpResponse httpResponse1 = httpclient1.execute(new HttpGet(params[0]));
                int statusCode1 = httpResponse1.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode1 == 200) {
                    String response1 = streamToString1(httpResponse1.getEntity().getContent());
                    parseResult1(response1);
                    result1 = 1; // Successful
                } else {
                    result1 = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result1;
        }

        @Override
        protected void onPostExecute(Integer result1) {
            // Download complete. Let us update UI
            if (result1 == 1) {
                mGridAdapterProduk.setGridData(mGridDataProduk);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString1(InputStream stream1) throws IOException {
        BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(stream1));
        String line1;
        String result1 = "";
        while ((line1 = bufferedReader1.readLine()) != null) {
            result1 += line1;
        }

        // Close stream
        if (null != stream1) {
            stream1.close();
        }
        return result1;
    }

    /**
     * Parsing the feed results and get the list
     *
     * @param result1
     */
    private void parseResult1(String result1) {
        try {
            JSONObject response1 = new JSONObject(result1);
            JSONArray posts1 = response1.optJSONArray(TAG_RESULT);
            GridItemDaftarProduk item1;

            for (int i = 0; i < posts1.length(); i++) {
                JSONObject post1 = posts1.optJSONObject(i);
                String stridproduk = post1.getString(TAG_IDPRODUK);
                String strnamaproduk = post1.getString(TAG_NAMAPRODUK);
                String strhargaproduk = post1.getString(TAG_HARGAPRODUK);
                String strbahanproduk = post1.getString(TAG_BAHANPRODUK);
                item1 = new GridItemDaftarProduk();
                item1.setIdproduk(stridproduk);
                item1.setNama(strnamaproduk);
                item1.setHarga(strhargaproduk);
                item1.setBahan(strbahanproduk);
                if (null != posts1 && posts1.length() > 0) {
                    JSONObject attachment = posts1.getJSONObject(i);
                    if (attachment != null)
                        item1.setGambar(attachment.getString(TAG_GAMBAR));
                }

                mGridDataProduk.add(item1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
